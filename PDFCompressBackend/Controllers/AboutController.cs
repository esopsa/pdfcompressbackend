﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PDFCompressBackend.Controllers {

  public class About {
    [JsonPropertyName("name")]
    public string Nm => Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);
    [JsonPropertyName("version")]
    public string Vr => "v" + Consts.SemVer;
    [JsonPropertyName("framework")]
    public string Fw => RuntimeInformation.FrameworkDescription;
    [JsonPropertyName("os")]
    public string Os => RuntimeInformation.OSDescription;
    [JsonPropertyName("timestamp")]
    public string Ts => DateTime.Now.ToUniversalTime().ToString("O");
  }

  [Route("[controller]")]
  [ApiController]
  public class AboutController : ControllerBase {

    [HttpGet]
    public JsonResult Get() {
      //Console.WriteLine("About/GET");
      JsonSerializerOptions jso = new JsonSerializerOptions { WriteIndented = true };
      JsonResult jr = new JsonResult(new About(), jso) { StatusCode = StatusCodes.Status200OK };
      return jr;
    }
  }
}
