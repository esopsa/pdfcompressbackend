﻿using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using Aspose.Pdf.Cloud.Sdk.Api;
using Aspose.Pdf.Cloud.Sdk.Client;
using Aspose.Pdf.Cloud.Sdk.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PDFCompressBackend.Controllers {

  public class Upload {
    [JsonPropertyName("path")]
    public string Url { get; set; }
  }

  [Route("[controller]")]
  [ApiController]
  public class CompressController : ControllerBase {

    [HttpPost]
    public JsonResult Post([FromQuery] int compressMode) {
      //Console.WriteLine("Compress/POST");
      // see webapps-develop\apps\Aspose.PDF.Shared\Controllers\DownloadController.cs
      string nm = Helper.MakeName();
      MemoryStream ms = new MemoryStream();
      Request.Form.Files[0].CopyTo(ms);
      ms.Position = 0;
      Configuration c = new Configuration(Helper.Key, Helper.Sid);
      PdfApi a = new PdfApi(c);
      JsonSerializerOptions jso = new JsonSerializerOptions { WriteIndented = true };

      // https://docs.aspose.cloud/pdf/working-with-files-and-storage-using-aspose-pdf-cloud/#tab91
      // https://github.com/aspose-pdf-cloud/aspose-pdf-cloud-dotnet/blob/master/docs/PdfApi.md#uploadfile
      FilesUploadResult fur = a.UploadFile(nm, ms);
      if (fur.Errors != null && fur.Errors.Count > 0)
        return new JsonResult(fur, jso) { StatusCode = StatusCodes.Status500InternalServerError };

      OptimizeOptions oo = Helper.MakeOptimizeOptions(compressMode);
      // https://github.com/aspose-pdf-cloud/aspose-pdf-cloud-dotnet/blob/master/docs/PdfApi.md#postoptimizedocument
      AsposeResponse ar = a.PostOptimizeDocument(nm, oo);
      if (ar.Code != 200)
        return new JsonResult(ar, jso) { StatusCode = StatusCodes.Status500InternalServerError };

      return new JsonResult(new Upload { Url = nm }, jso) { StatusCode = StatusCodes.Status200OK };
    }

    [HttpGet("{path}")]
    public FileStreamResult Get(string path) {
      //Console.WriteLine("Compress/GET");
      // see webapps-develop\apps\Aspose.PDF.Shared\Controllers\DownloadController.cs
      Configuration c = new Configuration(Helper.Key, Helper.Sid);
      PdfApi a = new PdfApi(c);
      MemoryStream ms = new MemoryStream();
      a.DownloadFile(path).CopyTo(ms);
      //a.DeleteFile(path);
      ms.Position = 0;
      // https://github.com/aspose-pdf-cloud/aspose-pdf-cloud-dotnet/blob/master/docs/PdfApi.md#downloadfile
      return new FileStreamResult(ms, "application/octet-stream");
    }
  }
}
