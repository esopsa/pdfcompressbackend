﻿using System;
using Aspose.Pdf.Cloud.Sdk.Model;

namespace PDFCompressBackend {

  internal static class Helper {

    internal static string Key;
    internal static string Sid;

    internal static void Init() {
      Key = _GetKey();
      Sid = _GetSid();
    }

    private static string _GetKey() {
      // https://dashboard.aspose.cloud/#/apps
      string r = Environment.GetEnvironmentVariable("PDF_COMPRESS_KEY");
      if (string.IsNullOrWhiteSpace(r))
        throw new Exception("no PDF_COMPRESS_KEY");
      return r;
    }

    private static string _GetSid() {
      // https://dashboard.aspose.cloud/#/apps
      string r = Environment.GetEnvironmentVariable("PDF_COMPRESS_SID");
      if (string.IsNullOrWhiteSpace(r))
        throw new Exception("no PDF_COMPRESS_SID");
      return r;
    }

    internal static string MakeName() {
      return Guid.NewGuid().ToString("N");
    }

    internal static OptimizeOptions MakeOptimizeOptions(int compressType) {
      OptimizeOptions oo = new OptimizeOptions {
        RemoveUnusedObjects = true,
        RemoveUnusedStreams = true,
        LinkDuplcateStreams = true,
        AllowReusePageContent = true
      };
      switch (compressType) {
        case 2:
          oo.CompressImages = true;
          oo.ImageQuality = 50;
          oo.ResizeImages = true;
          oo.MaxResolution = 300;
          break;
        case 3:
          oo.CompressImages = true;
          oo.ImageQuality = 25;
          oo.ResizeImages = true;
          oo.MaxResolution = 150;
          break;
      }
      return oo;
    }
  }
}
