using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace PDFCompressBackend {
  public class Program {
    public static void Main(string[] args) {
      Helper.Init();
      CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
      Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(webBuilder => {
          webBuilder.UseStartup<Startup>().
            ConfigureKestrel(options => {
              options.AllowSynchronousIO = true;
              options.ListenAnyIP(8910);
            });
        });
  }
}
