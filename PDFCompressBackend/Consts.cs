﻿namespace PDFCompressBackend {

  internal static class Consts {

    #region SemVer

    // https://semver.org/#spec-item-8
    private const int verMajor = 0;
    // https://semver.org/#spec-item-7
    private const int verMinor = 2;
    // https://semver.org/#spec-item-6
    private const int verPatch = 0;
    // https://semver.org/#spec-item-9
    private const string verPreRelease = "";
    // https://semver.org/#spec-item-10
    private const string verBuild = "";

    // semantic version (https://semver.org/)
    internal static readonly string SemVer;

    #endregion SemVer

    static Consts() {
      SemVer = $"{verMajor}.{verMinor}.{verPatch}";
      if (!string.IsNullOrWhiteSpace(verPreRelease))
        SemVer += $"-{verPreRelease}";
      if (!string.IsNullOrWhiteSpace(verBuild))
        SemVer += $"+{verBuild}";
    }
  }
}
